import { useQuery, gql } from "@apollo/client";

import {
    Flex, Center, Heading, Text, Box, Link
} from "@chakra-ui/react";
import { Card, CardHeader, CardBody, CardFooter } from '@chakra-ui/card';
// ------------------------------------------------------------------------
// Module 302, Mastery Assignment #3
//
// STEP 1: Replace this GraphQL query with a new one that you write.
//
// Need some ideas? We will brainstorm at Live Coding.
// ------------------------------------------------------------------------
const QUERY = gql`
query StakePool {
    activeStake {
      address 
    }
    delegations {
      address
    }
    blocks {
      number
    }
    rewards {
      amount
    }
  }
`;

export default function Mastery302dot3Sura() {
    const { data, loading, error } = useQuery(QUERY);
   
    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const myJSON = JSON.stringify(data, null, 2);
    const myArray = JSON.parse(myJSON);

    /*const DisplayTotalActiveStakes:any = Object.keys(myArray).length;*/

    const DisplayTotalActiveStakes = myArray.activeStake.map(
        (i:any) => {
            return (i.address);
        })

    const DisplayTotalDelegations = myArray.delegations.map(
        (j:any) => {
            return (j.address);
        }) 
        
    const DisplayTotalBlocksProduced = myArray.blocks.map(
        (k:any) => {
            return (k.number);
        })   

    const DisplayTotalRewardsAmount = myArray.rewards.map(
        (l:any) => {
            return (l.amount);
        })   

        var RewardsTotal = 0;
            for (let x=0; x < DisplayTotalRewardsAmount.length; x++){
                RewardsTotal += Math.floor(Number(DisplayTotalRewardsAmount[x]) / 1000000);
        }
        
   
    return (
      <Box
        color="#b1b1b3"
        m="5"
        p="5"
        border="1px"
        borderColor="gray.200"
        borderRadius="lg"
        bgGradient="linear(to-r, black, gray.600)"
      >
        <Heading size="md" p="2" color="purple.400">
          Sura's 302.3 Component
        </Heading>
        <Heading size="sm" px="2" pb="3" textColor="#6bdaff">
          Key Cardano Stakepool Numbers  <Box as="span" fontSize='xs' color="#f28455">(Pre Production)</Box>
        </Heading>
        <Box
          bg="#353536"
          p="5"
          border="1px"
          borderColor="gray.500"
          borderRadius="lg"
        >
          <Card>
            <CardBody fontFamily={"Calibri"} textColor={"#24e312"}>
              <Text p="1">
                Active Stakes:  <Box as="span" color="highlight">{DisplayTotalActiveStakes.length}</Box>
              </Text>
              <Text p="1">
                Total Delegations: <Box as="span" color="highlight">{DisplayTotalDelegations.length}</Box>
              </Text>
              <Text p="1">
                Blocks Produced: <Box as="span" color="highlight">{DisplayTotalBlocksProduced.length}</Box>
              </Text>
              <Text p="1">Total Rewards:  <Box as="span" color="highlight">{RewardsTotal}</Box> <Box as="span" fontSize='xs' color="#f28455"> ADA </Box></Text>
            </CardBody>
          </Card>
        </Box>
      </Box>
    );
}
