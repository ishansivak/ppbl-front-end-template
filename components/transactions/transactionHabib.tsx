//import type { NextPage } from "next";
//import { FormEvent, useState } from "react";
import { useForm } from "react-hook-form";
import { Transaction, BrowserWallet } from "@martifylabs/mesh";
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input, Text,
  NumberInput,
  NumberInputField,
  Button,
  Box,
  Heading
} from "@chakra-ui/react";

export default  function TransactionHabib() {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting }
  } = useForm();

  function onSubmit(values: {
    address(address: any, lovelace: any): unknown; lovelace: any; 
}) {
    return new Promise<void>((resolve) => {
      setTimeout(async () => {
        const wallet = await BrowserWallet.enable("eternl");
        const tx = new Transaction({ initiator: wallet })
          .sendLovelace(
            values.address,
            values.lovelace
          )
          ;
        const unsignedTx = await tx.build();
        const signedTx = await wallet.signTx(unsignedTx);
        const txHash = await wallet.submitTx(signedTx);
        alert(txHash) ;


        alert(JSON.stringify(values, null, 2));
        alert(values.address);
        resolve();
      }, 300);
    });
  }

  return (
    <Box p='5' bg='orange.100' border='1px' borderRadius='xl' fontSize='lg'>
       <Heading size='lg'> Sending Ada to </Heading>
    {/* <Text color="blue" align="center">minimum ada is 3</Text> */}
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl isInvalid={errors.address}>
      <FormLabel htmlFor="address">Recieving Address</FormLabel>
        <Input
          id="address" bg='white'
          placeholder="address"
          {...register("address", {
            required: "This is required",
            minLength: { value: 30, message: "Minimum length should be 30" }
          })}
        />
        <FormLabel htmlFor="name">lovelace amount</FormLabel>
        <NumberInput id="lovelace" defaultValue={3000000} min={3000000} bg='white' >
          <NumberInputField {...register("lovelace", {})} />
        </NumberInput>
        <Text as='sub' color="red" align="center">minimum 3 ada need to send</Text>
      </FormControl>
      <Button mt={4} colorScheme="teal" isLoading={isSubmitting} type="submit">
        Submit
      </Button>
    </form>
    </Box>
  );
}
